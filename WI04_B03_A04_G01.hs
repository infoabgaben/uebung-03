{-Gruppe 01: Ajein Loganathan, Lennart Ferlings, Sebastian Hinrichs
Blatt 02, Abgabe: 04.11.2019
-}

-- Die folgende Zeile können Sie ignorieren (aber nicht löschen)
module P034 where

-- ------------------Aufgabenteil a-----------------

--Rundet a auf k Stellen
round_k :: Double -> Int -> Double
round_k a k = normalize (fromIntegral(round (a * 10 ^ k))) k

--Bringt a in Normalform
normalize :: Double -> Int -> Double
normalize a k = a / (10 ^ k) 

-- ------------------Aufgabenteil b-----------------

--Runde die konvertierte Zahl
round_integral :: Integer -> Int -> Integer
round_integral n k = round(floating n k)

--Konvertiere die Ganzzahl in eine Gleitkommazahl
floating :: Integer -> Int -> Double
floating n k = fromIntegral(n) / (10 ^ k)

