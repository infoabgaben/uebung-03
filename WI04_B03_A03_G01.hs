{-Gruppe 01: Ajein Loganathan, Lennart Ferlings, Sebastian Hinrichs
Blatt 02, Abgabe: 28.10.2019
-}

--Es wird regnen, wenn es bewölkt ist und die Vögel tief fliegen
will_not_rain :: Bool -> Bool -> Bool
will_not_rain is_cloudy birds_fly_deep = not(is_cloudy && birds_fly_deep)

--Wenn keiner von beiden Schülern da ist, werden beide eine Fehlstunde bekommen, da sie keiner in die Namensliste einträgt
have_no_hours_of_absence :: Bool -> Bool -> Bool
have_no_hours_of_absence a_absent b_absent = not(a_absent && b_absent)

--Die Person mag die Pizza, wenn Schinken drauf ist und nicht gleichzeitig Oliven und Ananas
likes_the_pizza :: Bool -> Bool -> Bool -> Bool
likes_the_pizza has_ham has_olives has_pineapple = has_ham && not(has_olives && has_pineapple)

--Ein Gefangener wird aus dem Gefängnis entlassen wenn er gegen die anderen beiden Gefangenen
--aussagt. Wenn noch mindestens ein anderer Gefangener aussagt, müssen alle ihre Strafe absitzen
one_gets_out_of_jail :: Bool -> Bool -> Bool -> Bool
one_gets_out_of_jail a_testifies b_testifies c_testifies = (a_testifies && not(b_testifies || c_testifies))
                                                                || (b_testifies && not(a_testifies || c_testifies))
                                                                || (c_testifies && not(a_testifies || b_testifies))