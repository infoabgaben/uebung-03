{-Gruppe 01: Ajein Loganathan, Lennart Ferlings, Sebastian Hinrichs
Blatt 02, Abgabe: 04.11.2019
-}

--Die Funktion berechnet, wieviel es kostet für n Personen den Eintopf zu kochen
calculate_price :: Integer -> Double
calculate_price persons = calculate_price_of_onions persons + calculate_price_of_peppers persons + calculate_price_of_oil persons
                            + calculate_price_of_meat persons + calculate_price_of_tomato persons


---------Zwiebeln---------

--Berechnet die Menge an Zwiebeln die benötigt wird.
--Wenn die für 30 Leute gekocht wird, braucht man der Aufgabe entsprechend 8 Zwiebeln.
amount_of_onions :: Integer -> Double
amount_of_onions persons = fromIntegral(ceiling(fromIntegral(persons) * 0.25))

--Berechnet an Hand der Anzahl benötigter Zwiebeln die Anzahl der zu kaufenden Netze aus. Es muss immer aufgerundet werden,
--damit man nicht zu wenig Zwiebeln hat. Wenn 8 Zwiebeln benötigt werden, müssen dementsprechend zwei Netze gekauft werden.
amount_of_onionwebs :: Integer -> Integer
amount_of_onionwebs persons = ceiling(amount_of_onions persons / 6.0)

--Konstante der Kosten für ein Zwiebelnetz
price_of_onionweb :: Double
price_of_onionweb = 2.00

--Berechnet mit der Anzahl benötigter Zwiebelnetze und der Kosten pro Netz den Gesamtpreis für die Zwiebeln.
--Bei 2 Zwiebelnetzen liegt der Preis also bei 4.00
calculate_price_of_onions :: Integer -> Double
calculate_price_of_onions persons = fromIntegral(amount_of_onionwebs persons) * price_of_onionweb


---------Paprika---------

--Berechnet die Menge an Paprika die benötigt wird.
--Wenn die für 30 Leute gekocht wird, braucht man der Aufgabe entsprechend 8 Paprika.
amount_of_peppers :: Integer -> Integer
amount_of_peppers persons = ceiling(fromIntegral (persons) * 0.25)

--Konstante der Kosten für eine Paprika
price_of_pepper :: Double
price_of_pepper = 0.77

--Berechnet mit der Anzahl benötigter Paprika und der Kosten pro Paprika den Gesamtpreis
--Bei 8 Paprika die liegt der Preis also bei 6.16
calculate_price_of_peppers :: Integer -> Double
calculate_price_of_peppers persons = fromIntegral(amount_of_peppers persons) * price_of_pepper

---------Öl---------

--Berechnet die Menge an Öl die benötigt wird.
--Wenn die für 30 Leute gekocht wird, braucht man der Aufgabe entsprechend 210 ml Öl.
amount_of_oil :: Integer -> Integer
amount_of_oil persons = persons * 7

--Berechnet die Anzahl benötigter Ölflaschen.
--Für 210 ml Öl braucht man zum Beispiel nur eine Flasche.
amount_of_bottles :: Integer -> Integer
amount_of_bottles persons = ceiling(fromIntegral(amount_of_oil persons) / 750.0)

--Konstante der Kosten für eine Flasche Öl
price_of_bottle :: Double
price_of_bottle = 3.78

--Berechnet mit der Anzahl benötigter Flaschen und deren Kosten den Gesamtpreis
--Bei einer Flasche liegt er also bei 3.78
calculate_price_of_oil :: Integer -> Double
calculate_price_of_oil persons = fromIntegral(amount_of_bottles persons) * price_of_bottle


---------Meat---------

--Berechnet die Menge an Gyrosfleisch die benötigt wird.
--Wenn die für 30 Leute gekocht wird, braucht man der Aufgabe entsprechend 4500 g Fleisch.
amount_of_meat :: Integer -> Integer
amount_of_meat persons = persons * 150

--Berechnet an Hand der Anzahl benötigten Menge Fleisch die Anzahl der zu kaufenden Packete aus. Es muss immer aufgerundet werden,
--damit man nicht zu wenig Fleisch hat. Wenn 4500 g Fleisch benötigt werden, müssen dementsprechend 9 Packete gekauft werden.
amount_of_meatpacks :: Integer -> Integer
amount_of_meatpacks persons = ceiling(fromIntegral(amount_of_meat persons) / 500)

--Konstante der Kosten für ein Packet Fleisch
price_of_meatpack :: Double
price_of_meatpack = 2.24

--Berechnet mit der Anzahl benötigter Fleischpackete und der Kosten pro Packet den Gesamtpreis für das Fleisch.
--Bei 9 Fleischpacketen liegt der Preis also bei 20.16
calculate_price_of_meat :: Integer -> Double
calculate_price_of_meat persons = fromIntegral(amount_of_meatpacks persons) * price_of_meatpack


---------Tomaten---------

--Berechnet die Menge an Tomatendosen die benötigt wird.
--Wenn die für 30 Leute gekocht wird, braucht man der Aufgabe entsprechend 8 Tomatendosen.
amount_of_cans:: Integer -> Integer
amount_of_cans persons = ceiling(fromIntegral(persons) * 0.25)

--Konstante der Kosten für eine Dose
price_of_can :: Double
price_of_can = 4.99

--Berechnet mit der Anzahl benötigter Tomatendosen und der Kosten pro Dose den Gesamtpreis für die Tomaten.
--Bei 8 Dosen liegt der Preis also bei 39.92
calculate_price_of_tomato :: Integer -> Double
calculate_price_of_tomato persons = fromIntegral(amount_of_cans persons) * price_of_can